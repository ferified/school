<?php
include('getautos.php');
class Auto {
      function auto($merk, $prijs, $foto) {
        $this->merk = $merk;
        $this->prijs = $prijs;
        $this->foto = $foto;
      }
    }

function listAutos() {
global $result;
if ($result->num_rows == 0) {
 $html = "<center><b>Geen auto's gevonden</b></center>";

} else {
  $html = "";
 while($row = $result->fetch_assoc()) {
$auto = new auto($row['auto_merk'],  $row['auto_prijs'], $row['auto_foto']);
$html .= '<div id="auto" style="background-image:url(fotos/'.$auto->foto.');">'.'<p id="text">Merk: '.$auto->merk.'<br>Prijs: '.'&euro;'.number_format($auto->prijs,2,",",".").'</p></div>';
}
}
print $html;
}

listAutos();

?>
