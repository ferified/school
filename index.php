<html>
<head>
<title>Mr. Wheely</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="js/scripts.js"></script>
</head>
<body onload="showCars()">
<div class="jumbotron">
<center>
  <h1>Mr. Wheely</h1>
  <form>
<label>Merk:</label>
<select id="merk" name="merk">
<option value="">Alle merken</option>
<option value="Audi">Audi</option>
<option value="Ferrari">Ferrari</option>
<option value="Fiat">Fiat</option>
<option value="Mercedes">Mercedes</option>
<option value="Opel">Opel</option>
<option value="Volkswagen">Volkswagen</option>
</select>
<label>Minimum Prijs:</label>
<input id="minprijs" name="minprijs" type="number" min="0" maxlength="9" step="2500">
<label>Maximum Prijs:</label>
<input id="maxprijs" name="maxprijs" type="number" min="0" maxlength="9" step="2500">
</form>
<button value="zoeken" onclick="showCars()">Zoeken</button>
</center>
</div>
<div id="autoDisplay"></div>
</body>
</html>
